public class String1aii {

    //without using addition Ds : Bitwise operation on a 64 bit long integer with the ASCII of characters
    //use a standard string sorting algorithm: Not available, STRINGS ARE IMMUTABLE
    //take a long int of 64 bit and check using bitwise operation on 0-65 and 65-127 ASCII value characters

    static void check(String s){

        int n=0,m=0,a,count=0;

        for(int i=0;i<s.length();i++) {
            a = s.charAt(i);
            if (a < 64) {

                //checking if the character is repeated by checking its ASCII value where ASCII<64
                if ((n & (1 << a)) > 0) {
                    System.out.println("1st repeated character in string " + s +" is " + s.charAt(i));
                    break;
                } else {
                    count++;
                    n = n | (1 << a);
                }
            }
            else{
                //for characters with ASCII value greater than 63
                a = a-64;

                if((m & (1<<a))>0){
                    System.out.println("1st repeated character in string "+ s +"  is " + s.charAt(i));
                    break;
                }
                else{
                    count++;
                    m = m | (1<<a);
                }
            }
        }
        if(count==s.length())
        System.out.println("String "+ s +" has all unique characters");
    }


    public static void main(String args[]){
        String s1 = "sDdfduywiocjxncbywdgfwi";
        String s2 = "qwertyuiopasdfghjkl`1234567890-=";

        check(s1);
        check(s2);

    }
}
