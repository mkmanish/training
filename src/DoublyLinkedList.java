public class DoublyLinkedList {

    public class Node{
        int data;
        Node prev;
        Node next;
        Node(int value){
            data = value;
            prev = null;
            next = null;
        }
    }
    Node first,last;

    void add(int data){
        if(first==null || last==null){
            Node temp = new Node(data);
            first = temp;
            last = temp;
        }
    }

    public void insertFirst(int data){

        if(first==null)
            add(data);
        else {
            Node temp = new Node(data);
            temp.next = first;
            first.prev = temp;
            first = temp;
        }
    }

    void insertLast(int data){
        if(last==null)
            add(data);
        else {
            Node temp = new Node(data);
            last.next = temp;
            temp.prev = last;
            last = temp;
        }
    }

    void insertAfter(int key,int data){
        Node move = first;
        while(move.data != key)
            move = move.next;

        Node temp = new Node(data);
        temp.next = move.next;
        temp.prev = move;
        move.next = temp;
        temp.next.prev = temp;

    }

    void delete(int key){
        Node del = first;
        //If elelment to be deleted is the first element
        if(del.data==key){
            del = first;
            first = del.next;
            //for next and prev references   ?? Is it necessary as works smoothly in next step without this
            first.prev = null;
            del.next = null;
            //for releasing memory
            del = null;
        }
        else if(last.data == key){
            del = last;
            last = del.prev;
            del = null;
        }
        else{
            while(del.data != key)
                del = del.next;

            del.prev.next = del.next;
            del.next.prev = del.prev;
            del = null;
        }
    }

    void print(){
        Node temp = first;
        while(temp!=last){
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
        System.out.println(temp.data);
    }

    public static void main(String args[]){
        DoublyLinkedList dl = new DoublyLinkedList();
        dl.insertFirst(2);
        dl.insertFirst(1);
        dl.insertLast(3);
        dl.insertLast(4);
        dl.print();
        dl.delete(1);
        dl.insertAfter(2,0);
        dl.delete(3);
        dl.print();
    }
}
