/**
 * Normal Stack implementation with a Minimum function
 *
 * @author: Manish Kumar
 */

public class Stack3 {

    public static void main(String srgs[]){

        Stack3 stack = new Stack3();

        stack.pop();
        stack.peek();
        for(int i=41;i>11;i--)
            stack.push(i);

        stack.Min();
        stack.print();
        stack.pop();
        stack.peek();
        stack.print();
        stack.Min();

    }

    static int size=40;
    static int top=-1;
    int m;
    static int a[] = new int[size];

    /**
     * For fetching the current minimum value in the stack
     * @param data
     */

    void Min() {
        if(top==-1)
            return;
        m = Integer.MAX_VALUE;
        for(int i=0;i<=top;i++)
        {
            if(m>a[i])
                m = a[i];
        }
        System.out.println("Minimum value in the stack is: " + m);
    }

    /**
     * Method for pushing data in the stack
     * @param data
     */

    void push(int data){
        if(top == size-1){
            System.out.println("Stack is full, No more push allowed");
            return;
        }
        a[++top] = data;
    }

    /**
     * Method to pop data from stack
     */

    void pop(){
        if(top==-1){
            System.out.println("Stack is empty : pop() called");
        }
        else {
            System.out.println("Value at the top of stack is " + a[top]);
            top--;
        }
    }

    /**
     * Method to just have a look at the topmost value in stack
     */
    void peek(){
        if(top==-1)
            System.out.println("Stack is empty : peek() called");
        else
            System.out.println("Value at the top of stack is " + a[top]);
    }

    /**
     * Method to check if the stack is empty or not
     * @return
     */

    boolean isEmpty(){
        return (top==-1);
    }

    /**
     * Method to check if the stack is full
     * @return true/false
     */
    boolean isFull(){
        return (top==size-1);
    }

    /**
     * Method to print the entire stack in the the order of insertion
     */

    void print(){
        for(int i=0;i<=top;i++)
            System.out.print(a[i] + " ");
        System.out.println();
    }
}
