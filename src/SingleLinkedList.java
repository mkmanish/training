public class SingleLinkedList {

    class Node {
        int data;
        Node next; //a reference to node class

        //A constructor for making new nodes
        Node(int value) {
            data = value;
            next = null;
        }
    }

    Node head;  //A reference of Node class which will point to the beginning of list

    public void addFirst(int data) {
        Node temp = new Node(data);
            temp.next = head;
            head = temp;
    }

    public void addLast(int data) {
        if(head==null){
            addFirst(data);
        }
        else {
            Node temp = new Node(data);
            Node move = head;
            while (move.next != null) {
                move = move.next;
            }
            move.next = temp;
        }

    }
    void insertAfter(int key,int data){
        Node temp = head;
        Node temp2 = new Node(data);
        while(temp.data != key){
            temp = temp.next;
        }
        temp2.next = temp.next;
        temp.next = temp2;
    }

    void deleted(int key){
        Node move = head;
        Node temp;

        //head to be deleted
        if(move.data==key){
            temp = head;
            head = head.next;
            temp=null;
        }
        else {

            while (move.next.data != key) {
                move = move.next;
            }
            temp = move.next;
            move.next = temp.next;

            temp = null;  //freeing the memory
        }
    }

    void print(){
        Node move=head;
        if(move==null){
            System.out.println("list is empty ");
        }
        else{
            while(move.next != null)
            {
                System.out.print(move.data + " ");
                move = move.next;
            }
            System.out.println(move.data);
        }
    }


    public static void main(String args[]) {

        SingleLinkedList ll = new SingleLinkedList();
        ll.addFirst(3);
        ll.addFirst(1);
        ll.insertAfter(1,2);
        ll.addLast(4);
        System.out.print("Original List : ");
        ll.print();

        ll.deleted(1);
        System.out.print("List after deleting head : ");
        ll.print();
    }

}
