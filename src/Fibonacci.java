/**
 * A recursive approach to return fibonacci series
 * @author : Manish
 */

public class Fibonacci {

    /**
     * Implementing Fibonacci series using recursion
     * @param n
     * @return
     */

    int fibonacciSeries(int n){
        if(n==0 || n==1)
            return n;
        else if(n>0)
        return fibonacciSeries(n-1) + fibonacciSeries(n-2);
        else
        return 0;
    }

    /**
     * Main method returning Fibonacci values of all the values <= n
     * @param args
     */

    public static void main(String args[]){

        int n = 10;
        Fibonacci fb = new Fibonacci();

        for(int i=0;i<=n;i++)
            System.out.println(fb.fibonacciSeries(i));


    }
}
