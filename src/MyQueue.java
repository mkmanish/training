/**
 * Implementing Queue using two stack
 *
 * @author: Manish
 */

import java.util.*;

/**
 * Main method containing definition of queue and operations on it.
 */

public class MyQueue {

    public static void main(String args[]) {

        MyQueue queue = new MyQueue();
        for(int i=0;i<10;i++)
            queue.enqueue(i);

        queue.print();
        queue.dequeue();
        queue.dequeue();
        queue.print();

        queue.enqueue(100);
        queue.enqueue(200);

        queue.print();
        queue.dequeue();
        queue.print();
    }

    /**
     * Using Standard java collection Stack for implementing stack
     * He auto-boxing takes place from int to Integer
     * The concept of converting primitive type to object type is called as “boxing” and vice versa is “unboxing”.
     * Integer is a class and int behave as an object of Integer class
     */

    Stack<Integer> stack1 = new Stack<Integer>();
    Stack<Integer> stack2 = new Stack<>();

    /**
     * For enqueue we simply insert data in one of the stack
     * @param data
     */

    void enqueue(int data){
        stack1.push(data);
    }

    /**
     * for dequeue we first move data from one stack to another so that it follow the same order as the order of insertion
     * Then we pop()  the first element from the second stack and move the rest elements back to 1st stack
     */
    void dequeue(){
        while(!stack1.isEmpty()){
            stack2.push(stack1.pop());
        }
        System.out.println("Element dequeued is "+ stack2.pop());

        while(!stack2.isEmpty()){
            stack1.push(stack2.pop());
        }
    }

    /**
     * Since we need to print in the order of insertion we follow the same step as dequeue
     */
    void print(){
        while(!stack1.isEmpty()){
            stack2.push(stack1.pop());
        }
        while(!stack2.isEmpty()){
            System.out.print(stack2.peek() + " ");
            stack1.push(stack2.pop());
        }
        System.out.println();
    }
}
