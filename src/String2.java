public class String2 {

    //permutation: just store the count character's ASCII value in an array and match the two array

    static void permut(String s1,String s2){

        int a[] = new int[128];
        int b[] = new int[128];
        int count=0;

        for(int i=0;i<s1.length();i++)
            a[s1.charAt(i)]++;
        for(int i=0;i<s2.length();i++)
            b[s2.charAt(i)]++;

        for(int i=0;i<128;i++){
            if(a[i]!=b[i]){
                System.out.println(s1 +" and "+ s2 +" are not the permutation of each other");
                break;
            }
            else {
                count++;
            }
        }
        if(count==128)
            System.out.println(s1 +" and "+ s2 +" are permutation of each other");

    }

    public static void main(String args[]){
        String s1,s2,s3;
        s1 = "asdfghjkl";
        s2 = "lkjhasdfg";
        s3 = "asgdfhasy";

        permut(s1,s2);
        permut(s1,s3);
        permut(s2,s3);

    }
}
