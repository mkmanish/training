public class Stack1 {

    static int size=10,top=-1;
    static int a[] = new int[size];

    void push(int data){
        if(top == size-1){
            System.out.println("Stack is full, No more push allowed");
            return;
        }
        a[++top] = data;
    }

    void pop(){
        if(top==-1){
            System.out.println("Stack is empty : pop() called");
        }
        else {
            System.out.println("Value at the top of stack is " + a[top]);
            top--;
        }
    }
    void peek(){
        if(top==-1)
            System.out.println("Stack is empty : peek() called");
        else
            System.out.println("Value at the top of stack is " + a[top]);
    }

    boolean isEmpty(){
        return (top==-1);
    }
    boolean isFull(){
        return (top==size-1);
    }

    void print(){
        for(int i=0;i<=top;i++)
            System.out.print(a[i] + " ");
        System.out.println();
    }

    public static void main(String srgs[]){

        Stack1 s = new Stack1();

        s.pop();
        s.peek();
        for(int i=0;i<11;i++)
            s.push(i);

        s.print();
        s.pop();
        s.peek();

    }
}
