public class String1ai {

    //finding uniqueness of character in a string
    // since we only have 128 different characters frequently used(ranges upto 2^16 = 65536)
    // we can use character's ASCII code as the hash value to map them
    //Useful only if string length is small

    //Alternate: we can use a set, before inserting characters of sting into set check if character already exist: O(1) lookup operation in set

    static void check(String s){

        int a[] = new int[128];
        int count=0;

        for(int i=0;i<s.length();i++){
            if(a[s.charAt(i)]==1) {
                System.out.println("1st repeated character in string s1 is " + s.charAt(i));
                break;
            }
            else {
                count++;
                a[s.charAt(i)] = 1;
            }
        }
        if(count==s.length())
            System.out.println("String s1 has all unique characters");

    }


    public static void main(String args[])
    {
        String s1 = "sddfuywiocjxncbywdgfwi";
        String s2 = "qwertyuiopasdfghjkl`1234567890-=";

       check(s1);
       check(s2);
    }
}
