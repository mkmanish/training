import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String args[])throws IOException{

        String1ai();
        String1aii();
        String2();
        String3();

        SingleLinkedList();
        DoublyLinkedList();

        LinkedList3();

        LinkedList4();
        LinkedList5();

        Fibonacci();
        MyQueue();
        Queue1();

        Stack1();
        Stack2();
        Stack3();

        FileOperations();

    }

    static void String1ai(){
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Check Uniqueness of character in string : No constrain");

        String s1 = "sddfuywiocjxncbywdgfwi";
        String s2 = "qwertyuiopasdfghjkl`1234567890-=";

        String1ai string = new String1ai();
        string.check(s1);
        string.check(s2);
    }

    static void String1aii(){
        String s1 = "sDdfduywiocjxncbywdgfwi";
        String s2 = "qwertyuiopasdfghjkl`1234567890-=";
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Check Uniqueness of character in string : No additional Data Structure allowed : Bitwise");


        String1aii string = new String1aii();
        string.check(s1);
        string.check(s2);

    }

    static void String2(){
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Check if one string is permutation of another string");

        String s1,s2,s3;
        s1 = "asdfghjkl";
        s2 = "lkjhasdfg";
        s3 = "asgdfhasy";
        String2 string = new String2();

        string.permut(s1,s2);
        string.permut(s1,s3);
        string.permut(s2,s3);

    }

   static void String3() {
       System.out.println("-------------------------------------------------------------------------------------");
       System.out.println("Replacing spaces in a string with $");

        String s = "g iyk uyjd nb ur6 k";
        String3.replace(s);
    }

    static void SingleLinkedList() {

        System.out.println("\n-------------------------------------------------------------------------------------");
        System.out.println("Implementing Singly linkedList");

        SingleLinkedList ll = new SingleLinkedList();
        ll.addFirst(3);
        ll.addFirst(1);
        ll.insertAfter(1,2);
        ll.addLast(4);
        System.out.print("Original List : ");
        ll.print();

        ll.deleted(1);
        System.out.print("List after deleting head : ");
        ll.print();
    }

    static void DoublyLinkedList(){
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Implementing Doubly linkedList");

        DoublyLinkedList dl = new DoublyLinkedList();
        dl.insertFirst(2);
        dl.insertFirst(1);
        dl.insertLast(3);
        dl.insertLast(4);
        dl.print();
        dl.delete(1);
        dl.insertAfter(2,0);
        dl.delete(3);
        dl.print();
    }

    static void LinkedList3() {
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Removing Duplicates from Singly linkedList");

        LinkedList3 ll = new LinkedList3();

        for (Integer i = 0; i < 10; i++)
            ll.add(i);

        for (Integer i = 2; i <= 10; i+=2)
            ll.add(i);
        System.out.println("Original list is : ");
        ll.printInteger();

        System.out.println("List after removing duplicate data");
        ll.removeDuplicates();
        ll.printInteger();
    }

    static void LinkedList4() {
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Kth element from last in a linked list");

        LinkedList4 ll = new LinkedList4();
        ll.addFirst(2);
        ll.addFirst(1);
        for(int i=3;i<=20;i++)
            ll.addLast(i);

        System.out.print("Original List : ");
        ll.print();

        int k=11;
        ll.kthElement(k);

    }

    static void LinkedList5() {
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Partitioning list around a given value");

        LinkedList5 ll = new LinkedList5();
        ll.insertElement();

        System.out.print("Original List : ");
        ll.print();

        int k = 15;
        ll.partition(k);
        ll.print();
    }

    static void Fibonacci(){
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Fibonacci Series");

        int n = 10;
        Fibonacci fb = new Fibonacci();

        for(int i=0;i<=n;i++)
            System.out.println(fb.fibonacciSeries(i));

    }

    static void MyQueue() {
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Implementing Queue using two stack");

        MyQueue queue = new MyQueue();
        for(int i=0;i<10;i++)
            queue.enqueue(i);

        queue.print();
        queue.dequeue();
        queue.dequeue();
        queue.print();

        queue.enqueue(100);
        queue.enqueue(200);

        queue.print();
        queue.dequeue();
        queue.print();
    }

    static void Queue1(){
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("A simple structure to implement Queue having methods Enqueue, Dequque, Is Empty, Is Full");

        int a[] = new int[10];

        Queue1 queue = new Queue1();
        queue.dequeue(a);
        queue.enqueue(a,3);

        for(int i=0; i<10;i+=2)
            queue.enqueue(a,i);

        queue.print(a);
        queue.dequeue(a);
        queue.dequeue(a);

        //For checking the element insertion once a normal array is Full
        for(int i=10; i<20;i+=2)
            queue.enqueue(a,i);

        queue.print(a);

        queue.dequeue(a);
        queue.dequeue(a);
        queue.dequeue(a);

        if(queue.isEmpty())
            System.out.println("Queue is empty");
        else
            System.out.println("Queue is not empty");


        queue.print(a);
        System.out.print("front @ " + queue.front + " rear @ " + queue.rear);

    }

    static void Stack1(){
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Basic Stack Implementation");
        Stack1 s = new Stack1();

        s.pop();
        s.peek();
        for(int i=0;i<11;i++)
            s.push(i);

        s.print();
        s.pop();
        s.peek();

    }

    static void Stack2(){
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("Implementing K stacks using an Array of Size n");

        int noOfStack = 3;
        int sizeOfArray = 10;

        Stack2 stack = new Stack2(noOfStack,sizeOfArray);

        stack.push(10,0);
        stack.push(20,0);
        stack.push(100,1);
        stack.push(200,1);
        stack.push(1000,2);
        stack.push(2000,2);
        stack.printArray(sizeOfArray);

        stack.printStack(0);
        stack.printStack(1);
        stack.printStack(2);

        stack.pop(1);
        stack.pop(2);
        stack.printArray(sizeOfArray);

        stack.push(30,0);
        stack.push(40,0);
        stack.printArray(sizeOfArray);

        stack.pop(0);
        stack.pop(1);
        stack.pop(2);
        stack.printArray(sizeOfArray);

        stack.printStack(0);
        stack.printStack(1);
        stack.printStack(2);

    }

    static void Stack3(){
        System.out.println("-------------------------------------------------------------------------------------");
        System.out.println("A min function in an stack to give present minimum element");
        Stack3 stack = new Stack3();

        stack.pop();
        stack.peek();
        for(int i=41;i>11;i--)
            stack.push(i);

        stack.Min();
        stack.print();
        stack.pop();
        stack.peek();
        stack.print();
        stack.Min();

    }
     static void FileOperations() throws IOException{
         System.out.println("-------------------------------------------------------------------------------------");
         System.out.println("Basics of File handling : Creating,deleting,reading and writing of files");
        FileOperations newFile = new FileOperations();

        File file = newFile.createFile("/home/manish/training/ReadWrite/","newFile.txt");

        System.out.println("path of file is " + newFile.getPath(file));

        newFile.writeFile(file);
        String content = newFile.readFile(file);
        String oldContent = newFile.updateFileByWords(file, content,"This","That");
        String updatedContent = newFile.updateFileByLine(file,oldContent,"Typing the 2nd line which will be" +
                " updated later on","Updated the 2nd line");
    }
}