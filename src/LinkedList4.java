
public class LinkedList4 {

    //Kth element from last, Idea: Taken two reference node such that 1st node is ahead of 2nd by by (k-1)nodes
    //When 1st node reaches the end of the list by the time 2nd node will point to Kth element from last

    class Node {
        int data;
        Node next;

        Node(int value) {
            data = value;
            next = null;
        }
    }

    Node head;

    void kthElement(int k){
        Node first = head;
        Node second = head;
        Node move = head;
        int length = 0;
        while(move!=null){
            move = move.next;
            length++;
        }
        if(k>length)
            System.out.println("Value of K is greater than length of linkedlist");
        else {
            for (int i = 1; i < k; i++)
                first = first.next;

            while(first.next!=null){
                first = first.next;
                second = second.next;
            }
            System.out.println(k +"th element from last is " + second.data);
        }
    }


    public void addFirst(int data) {
        Node temp = new Node(data);
        temp.next = head;
        head = temp;
    }

    public void addLast(int data) {
        if(head==null){
            addFirst(data);
        }
        else {
            Node temp = new Node(data);
            Node move = head;
            while (move.next != null) {
                move = move.next;
            }
            move.next = temp;
        }

    }

    void print(){
        Node move=head;

            while(move.next != null)
            {
                System.out.print(move.data + " ");
                move = move.next;
            }
            System.out.println(move.data);

    }

    public static void main(String args[]) {

        LinkedList4 ll = new LinkedList4();
        ll.addFirst(2);
        ll.addFirst(1);
        for(int i=3;i<=20;i++)
            ll.addLast(i);

        System.out.print("Original List : ");
        ll.print();

        int k=11;
        ll.kthElement(k);

    }

}