public class LinkedList5 {

    //Arrange according to a specific value : Take a new list and just add at 1st or last of the list
    //Alter: If space complexity is issue : In the same list whenever see a smaller value tha K add same Node at front
    //For a larger value than k add it to the last of list : No extra space needed and Constant time add & delete

    //If we wish to maintain the same order

    static class Node {
        int data;
        Node next;

        Node(int value) {
            data = value;
            next = null;
        }
    }

    Node head;

    void partition(int k){
        Node temp = head,end,del,last = head,prev=head;
        while(last.next!=null)
            last = last.next;
        end = last;


        while(temp!=end){
            del = temp;
            if(temp.data > k){

                if(temp == head)
                {
                    head = head.next;
                    temp = temp.next;
                    del.next = null;
                    addLast(del);
                    prev = head;
                }
                else {
                    prev.next = temp.next;
                    temp = temp.next;
                    del.next = null;
                    addLast(del);
                }
            }
            else if(temp.data < k){
                if(temp == head){
                    temp = temp.next;
                }
                else {
                    prev.next = temp.next;
                    temp = temp.next;
                    del.next=null;
                    addFirst(del);
                }
            }
            else if(temp.data == k) {
                prev = temp;
                temp = temp.next;
            }
        }

    }

    public void addFirst(Node key) {
        key.next = head;
        head = key;
    }

    public void addLast(Node node) {
        if(head==null){
            addFirst(node);
        }
        else {
            Node move = head;
            while (move.next != null) {
                move = move.next;
            }
            move.next = node;
        }

    }

    void print(){
        Node move=head;

        while(move.next != null)
        {
            System.out.print(move.data + " ");
            move = move.next;
        }
        System.out.println(move.data);

    }

    public static void main(String args[]) {

        LinkedList5 ll = new LinkedList5();
        int a[] = {41,6,8,21,34,56,3,5,7,9,190,23,56,34,78,90,3};
        for(int i=0;i<a.length;i++) {
            Node node = new Node(a[i]);
            ll.addLast(node);
        }

        System.out.print("Original List : ");
        ll.print();

        int k = 15;
        ll.partition(k);
        ll.print();
    }

}