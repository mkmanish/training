import java.util.*;

//For removing duplicates in the list if list size is small we can use a hash table
//For larger dataset we can use a Set as lookup operation in set is O(1)

public class LinkedList3 {

    Node head;
    class Node{
        int data;
        Node next;
        Node(int value){
            data = value;
            next = null;
        }
    }

    void add(int data){
        Node temp = new Node(data);
        if(head==null)
        {
            temp.next = head;
            head = temp;
            return;
        }
        Node move=head;
        while(move.next!=null)
            move = move.next;

        move.next = temp;
    }

    Node delete(Node n){
        Node temp = head;
        while(temp!=null && temp.next != n)
            temp = temp.next;

        temp.next = n.next;
        n = null;
        return temp;

    }

    void print(){
        Node temp = head;
        while(temp.next != null) {
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
        System.out.println(temp.data);
    }

    public static void main(String args[]){
        LinkedList3 ll = new LinkedList3();
        for(int i=0;i<10;i++)
            ll.add(i);

        for(int i=2;i<10;i++)
            ll.add(i);

        ll.print();

        Set<Integer> s = new HashSet<Integer>();
        Node temp = ll.head;

        while(temp!=null){
            if(s.contains(temp.data)){
                temp = ll.delete(temp);
            }
            else{
                s.add(temp.data);
            }
            temp = temp.next;
        }

        ll.print();
    }

}
