/**
 * Implementing any desired number of stack in an array
 *
 * @author: Manish
 */

public class Stack2 {


    /**
     * Main method
     * @param args
     */
    public static void main(String args[]){


        int noOfStack = 3;
        int sizeOfArray = 10;

        Stack2 stack = new Stack2(noOfStack,sizeOfArray);

        stack.push(10,0);
        stack.push(20,0);
        stack.push(100,1);
        stack.push(200,1);
        stack.push(1000,2);
        stack.push(2000,2);
        stack.printArray(sizeOfArray);

        stack.printStack(0);
        stack.printStack(1);
        stack.printStack(2);

        stack.pop(1);
        stack.pop(2);
        stack.printArray(sizeOfArray);

        stack.push(30,0);
        stack.push(40,0);
        stack.printArray(sizeOfArray);

        stack.pop(0);
        stack.pop(1);
        stack.pop(2);
        stack.printArray(sizeOfArray);

        stack.printStack(0);
        stack.printStack(1);
        stack.printStack(2);

    }

    /**
     * variable and array declaration
     */

    //Defining a variable "free" which will indicate the next available free location in the array
    int free = 0;  //initial free location is the stating of array

    int array[];
    int top[]; //for storing top of each stack
    int pointer[];  // for storing index of previous element of the satck

    /**
     * A constructor for initializing arrays based on required no os stack and Full array length
     *
     * @param noOfStack
     * @param sizeOfArray
     */


    Stack2(int noOfStack, int sizeOfArray){

        array = new int[sizeOfArray];
        top = new int[noOfStack];
        pointer = new int[sizeOfArray];

        //Initializing the pointer array as "index+1" so that while pushing element for the first time we can move ahead in array
        for(int i=0;i<sizeOfArray-1;i++)
            pointer[i] = i+1;

        //Making the last pointer location to be "-1" for indicating : No more space available
        pointer[sizeOfArray-1] = -1;

        //initializing top of each of each stack as -1, same approach as used in any normal stack implementation
        for(int i=0;i<noOfStack;i++)
            top[i] = -1;
    }

    /**
     * Method for printing the original array
     * @param sizeOfArray
     */

    void printArray(int sizeOfArray){
        System.out.println("The array looks like :");

        for(int i=0;i<sizeOfArray;i++)
            System.out.print(array[i] + " ");
        System.out.println();
    }

    /**
     * Method for printing the stack
     * @param stackN
     */
    void printStack(int stackN){
        int i = top[stackN];

        if(top[stackN] != -1) {
            System.out.print("stack "+ stackN + " looks like : ");
            while (pointer[i] != -1) {
                System.out.print(array[i] + " ");
                i = pointer[i];
            }
            System.out.println(array[i] + " ");
        }
        else
            System.out.println("Stack " + stackN +" is empty");
    }

    /**
     * Method for implementing push operation in array in space efficient manner
     * @param data
     * @param stackN
     */

    void push(int data, int stackN){

        //Checking if there is empty space or not
        if(free==-1) {
            System.out.println("Array is full");
            return;
        }

        int i = free;               //Storing locating free index
        free = pointer[i];          //locating next free index
        pointer[i] = top[stackN];   //storing position of previous element of the Nth satck
        top[stackN] = i;            //Updating position of top of Nth stack
        array[i] = data;            //Inserting data in the free location
    }

    /**
     * Method for implementing pop() operation and storing the empty space positions
     * @param stackN
     */
    void pop(int stackN){
        int i = top[stackN];
        top[stackN] = pointer[i];
        pointer[i] = free;
        free = i;

        System.out.println("Poped element from stack "+ (stackN+1) + " is : " + array[i]);
        array[i] = 0;
    }

}