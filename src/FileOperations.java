/**
 * Basics of File handling : Creating,deleting,reading and writing of files
 *
 * @author : Manish
 */

import java.io.*;

public class FileOperations {

    public static void main(String args[]) throws IOException {

        FileOperations newFile = new FileOperations();

        File file = newFile.createFile("/home/manish/training/ReadWrite/","newFile.txt");

        System.out.println("path of file is " + newFile.getPath(file));

        newFile.writeFile(file);
        String content = newFile.readFile(file);
        String oldContent = newFile.updateFileByWords(file, content,"This","That");
        String updatedContent = newFile.updateFileByLine(file,oldContent,"Typing the 2nd line which will be" +
                " updated later on","Updated the 2nd line");
    }

    /**
     * Method for creating file with given directory path and desired file Name
     * @param path : path of directory
     * @param fileName
     * @return
     */

    public File createFile(String path, String fileName){

        //Creating  a file named newFile.txt
        File file = new File(path,fileName);

        //If a file with same name already exit delete that file
        if(file.exists())
            file.delete();

        //creating a new file
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file;
    }

    /**
     * Method for obtaining file path
     * @param file
     * @return
     */
    public String getPath(File file){
        return file.getPath();
    }


    /**
     * Method for writing in the generated file
     * @param file
     * @throws IOException
     */

    public void writeFile(File file)throws IOException{

        System.out.print("\n Writing in the file......... \n");
        FileWriter writer = new FileWriter(file);
        writer.write("This file is created by me and I am witing here to check the context  \n");
        writer.write("Typing the 2nd line which will be updated later on");
        writer.close();
    }

    /**
     * Method for reading content of a file
     * @param file
     * @return
     * @throws IOException
     */

    public String readFile(File file) throws IOException{

        //File Reading1 : Reading Character by character
        System.out.println("\nReading file Character by Character");
        FileReader filereader = new FileReader(file);
        int character;
        //Reading character by character
        while((character = filereader.read()) > 0){
            System.out.print((char)character);
        }
        filereader.close();

        //File Reading2: Reading Line by Line : We can also read character wise if we use just use .read
        System.out.println("\n \nReading file using BufferReader");

        BufferedReader reader = new BufferedReader(new FileReader(file));
        String oldLine;
        String oldContent = "";

        while((oldLine = reader.readLine())!=null){
            oldContent += oldLine + "\r\n";
        }
        reader.close();
        System.out.println(oldContent);

        return oldContent;
    }

    /**
     * Method for updating file when an entire line is to be updated
     * @param file  : File to be updated
     * @param oldContent  : Previous data in file
     * @param oldLine
     * @param newLine
     * @return
     * @throws IOException
     */

    public String updateFileByLine(File file, String oldContent, String oldLine, String newLine) throws IOException {

            //Update by line
            FileWriter update = new FileWriter(file);
            String updated = oldContent.replaceAll(oldLine,newLine);
            update.write(updated);
            update.close();

            //Reading the updated text
            BufferedReader read = new BufferedReader(new FileReader(file));
            String line,updatedText = "";
            while((line = read.readLine()) != null)
            {
                updatedText += line + "\r\n";
            }
            read.close();
            System.out.println("printing the updated lines from file  \n" + updatedText);

            return updatedText;
    }

    /**
     * Method for updating file when some words are to be updated
     * @param file  : File to be updated
     * @param oldContent  : Previous data in file
     * @param oldWord
     * @param newWord
     * @return
     * @throws IOException
     */

    public String updateFileByWords(File file, String oldContent, String oldWord, String newWord)throws  IOException{

        //Updating the content of file
        FileWriter updater = new FileWriter(file);

        //Update by words
        String updatedContent = oldContent.replace(oldWord,newWord);
        updater.write(updatedContent);
        updater.close();

        return updatedContent;
    }

    /**
     * Method for deleting the file
     */
     public void deleteFile(File file){
         file.delete();
     }
    /*
            //Reading an existing file
            //An instance of a file class is immutable
            File newFile = new File("/home/manish/training/ReadWrite/myFile.txt");

            if(newFile.exists()){
                System.out.println("File is readable : " + newFile.canRead());
                System.out.println("File can be written : "+ newFile.canWrite());
            }
            */

}
